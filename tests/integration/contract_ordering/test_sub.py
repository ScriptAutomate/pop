import pop.hub


def test_single_module_contract_ordering(
    capsys,
):
    hub = pop.hub.Hub()
    hub.pop.sub.add(
        pypath="tests.integration.contract_ordering.single_module",
        subname="test_sub",
        default_contracts=["default"],
    )

    expected_result = [
        "thing-pre-test-fn",
        "thing-pre",
        "init-pre-test-fn",
        "init-pre",
        "default-pre-test-fn",
        "default-pre",
        "dunder-pre-test-fn",
        "dunder-pre",
        "recursive-thing-pre-test-fn",
        "recursive-thing-pre",
        "recursive-init-pre-test-fn",
        "recursive-init-pre",
        "recursive-dunder-pre-test-fn",
        "recursive-dunder-pre",
        "thing-pre-call-test-fn",
        "test-fn",
        "thing-post-call-test-fn",
        "thing-post-test-fn",
        "thing-post",
        "init-post-test-fn",
        "init-post",
        "default-post-test-fn",
        "default-post",
        "dunder-post-test-fn",
        "dunder-post",
        "recursive-thing-post-test-fn",
        "recursive-thing-post",
        "recursive-init-post-test-fn",
        "recursive-init-post",
        "recursive-dunder-post-test-fn",
        "recursive-dunder-post",
    ]
    expected_output = "\n".join(expected_result) + "\n"

    hub.pop.sub.load_subdirs(hub.test_sub, recurse=True)

    actual_result = hub.test_sub.thing.test_fn()

    output = capsys.readouterr()
    assert output.out == expected_output
    assert actual_result == expected_result
