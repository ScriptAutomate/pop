"""
used to test the sub init system
"""
# pylint: disable=undefined-variable


def __init__(hub):
    """
    Add a value to the context
    """
    hub.context["init"] = True
    hub._.inited()


def inited(hub):
    return "init" in hub.context
